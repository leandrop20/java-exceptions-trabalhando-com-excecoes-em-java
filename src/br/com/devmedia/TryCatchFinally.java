package br.com.devmedia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TryCatchFinally {

	public static void main(String[] args) {
//		tryCatch1();
//		tryCatch2();
//		manyCatches();
//		withFinally();
//		throwException();
//		throwsException();
//		customException();
//		withThreads();
//		assertion();
	}

	private static void tryCatch1() {
		try {
			int vet[] = new int[10];
			System.out.println("Acessando elemento 20:" + vet[20]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Exce��o Lan�ada: " + e.getMessage());
		}
	}
	
	private static void tryCatch2() {
		try {
			int vet[] = new int[10];
			System.out.println("Acessando elemento 20:" + vet[20]);
		} catch (Exception e) {
			System.out.println("Exce��o Lan�ada: " + e.getMessage());
		}
	}
	
	private static void manyCatches() {
		try {
			int a[] = new int[10];
			System.out.println("Acessando elemento 20:" + a[20]);
		} catch (NullPointerException e) {
			System.out.println("NullPointerException: " + e.getMessage());
		} catch (ArrayIndexOutOfBoundsException e2) {
			System.out.println("ArrayIndexOutOfBoundsException: " + e2.getMessage()); 
		} catch (RuntimeException e3) {
			System.out.println("RuntimeException: " + e3.getMessage());
		} catch (Exception e4) {
			System.out.println("Exception: " + e4.getMessage());
		} catch (Error e5) {
			System.out.println("Error: " + e5.getMessage());
		} catch (Throwable e6) {
			System.out.println("Throwable: " + e6.getMessage());
		}
	}
	
	private static void withFinally() {
		class Test {
			public void copiarArquivo(String origem, String destino) {
				FileInputStream in = null;
				FileOutputStream out = null;
				
				try {
					File f1 = new File(origem);
					File f2 = new File(destino);
					in = new FileInputStream(f1);
					out = new FileOutputStream(f2);
					byte[] buf = new byte[1024];
					int len;
					while ((len = in.read(buf)) > 0) {
						out.write(buf, 0, len);
					}
				} catch (FileNotFoundException e) {
					System.out.println("FileNotFoundException: " + e.getMessage());
				} catch (IOException e2) {
					System.out.println("IOException: " + e2.getMessage());
				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (Exception e) {}
					}
					if (out != null)  {
						try {
							out.close();
						} catch (Exception e) {}
					}
				}
			}
		}
		new Test().copiarArquivo("teste.txt", "teste2.txt");
	}

	private static void throwException() {
		try {
			throw new Error("Erro lan�ado");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void throwsException() {
		class Test {
			public void testing() throws FileNotFoundException {
				FileInputStream in = null;
				try {
					in = new FileInputStream(new File("test.txt"));
				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (Exception e) {}
					}
				}
			}
		}
		try {
			new Test().testing();
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException: " + e.getMessage());
		}
	}
	
	private static void customException() {
		class Test {
			public void testing(int value) throws CustomException {
				if (value == 0)
					throw new CustomException(0, "Minha Exce��o", "MSG");
			}
		}
		try {
			new Test().testing(0);
		} catch (CustomException e) {
			e.printStackTrace();
			System.out.println("Codigo: " + e.getCodigoErro());
			System.out.println("Mensagem Auxiliar: " + e.getMensagemAuxiliar());
		}
	}
	
	private static void withThreads() {
		class Thread2 extends Thread {
			public void run() {
				synchronized (this) {
					try {
						wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
		class Thread1 extends Thread {
			public void run() {
				Thread2 thread2 = new Thread2();
				thread2.start();
				synchronized (thread2) {
					thread2.interrupt();
				}
			}
		}
		Thread1 thread1 = new Thread1();
		thread1.start();
	}
	
	private static void assertion() {
		int x = 0;
		assert x > 0;
	}
	
}