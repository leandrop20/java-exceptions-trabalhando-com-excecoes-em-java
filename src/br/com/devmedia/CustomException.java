package br.com.devmedia;

public class CustomException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private int codigoErro = 0;
	private String mensagemAuxiliar;
	
	public CustomException(int codigoErro, String mensagem, String mensagemAuxiliar) {
		super(mensagem);
		this.codigoErro = codigoErro;
		this.mensagemAuxiliar = mensagemAuxiliar;
	}

	public int getCodigoErro() {
		return codigoErro;
	}

	public void setCodigoErro(int codigoErro) {
		this.codigoErro = codigoErro;
	}

	public String getMensagemAuxiliar() {
		return mensagemAuxiliar;
	}

	public void setMensagemAuxiliar(String mensagemAuxiliar) {
		this.mensagemAuxiliar = mensagemAuxiliar;
	}

}