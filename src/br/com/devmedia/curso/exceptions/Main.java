package br.com.devmedia.curso.exceptions;

public class Main {

	public static void main(String[] args) {
		
		try {
			ItemVenda camisa = new ItemVenda();
			camisa.setDescricao("Camisa Polo");
			camisa.setPreco(30.0);
			
			ItemVenda bone = new ItemVenda();
			bone.setDescricao("Bon� Infantil");
			bone.setPreco(21.0);
			
			Venda venda = new Venda();
			venda.adicionar(camisa);
			venda.adicionar(bone);
			
			System.out.println("Total de venda: " + venda.getTotal());
		} catch (VendaException e) {
			System.out.println("Erro ao processar total: " + e.getMessage() +
					" - C�digo: " + e.getCodigo());
		} catch (IllegalArgumentException e) {
			System.out.println("Erro ao processar total: " + e.getMessage());
		} finally {
			System.out.println("Processamento encerrado!");
		}
	}

}