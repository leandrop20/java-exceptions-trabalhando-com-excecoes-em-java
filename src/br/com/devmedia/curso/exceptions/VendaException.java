package br.com.devmedia.curso.exceptions;

public class VendaException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	private int codigo;

	public VendaException(int codigo, String message) {
		super(message);
		this.codigo = codigo;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
}