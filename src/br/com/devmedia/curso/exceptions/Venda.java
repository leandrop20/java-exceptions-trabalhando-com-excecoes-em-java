package br.com.devmedia.curso.exceptions;

import java.util.ArrayList;
import java.util.List;

public class Venda {

	List<ItemVenda> itens = new ArrayList<ItemVenda>();
	
	public void adicionar(ItemVenda itemVenda) {
		if (itemVenda == null) {
			throw new VendaException(500, "Item n�o pode ser nulo");
		}
		itens.add(itemVenda);
	}
	
	public double getTotal() {
		double total = 0;
		
		for (ItemVenda item : itens) {
			total += item.getPreco();
		}
		
		return total;
	}
	
}